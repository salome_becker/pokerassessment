package com.salome.factories.cards;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.salome.domain.cards.Deck;

public class DeckFactoryTest
{

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void testCreateDeck()
	{
		Deck deck = DeckFactory.getInstance().createDeck(false);
		assertEquals(52, deck.cardsLeft());
	}

}
