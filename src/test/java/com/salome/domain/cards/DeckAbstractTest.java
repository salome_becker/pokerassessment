package com.salome.domain.cards;

import static org.junit.Assert.*;

import org.dsaw.poker.engine.Card;
import org.junit.Before;
import org.junit.Test;

public class DeckAbstractTest
{

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void testCardsLeft()
	{
		Deck deck = new Deck(false);
		assertEquals(52, deck.cardsLeft());
	}

	@Test
	public void testDealCard()
	{
		Deck deck = new Deck(false);
		deck.dealCard();
		assertEquals(51, deck.cardsLeft());
	}

	@Test
	public void testHasJokers()
	{
		Deck deck = new Deck(true);
		assertEquals(54, deck.cardsLeft());
	}

}
