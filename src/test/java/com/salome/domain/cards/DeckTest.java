package com.salome.domain.cards;

import static org.junit.Assert.*;

import org.dsaw.poker.engine.Card;
import org.junit.Before;
import org.junit.Test;

public class DeckTest
{

	private static final int HAND_SIZE = 5;

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void testDeckWithAndWithoutJokers()
	{
		Deck deck = new Deck(false);
		assertEquals(52, deck.cardsLeft());
		
		deck = new Deck(true);
		assertEquals(54, deck.cardsLeft());		
	}
	
	@Test
	public void testShuffle()
	{
		Deck deck = new Deck(false);
		Card card1 = deck.getCardAt(3);
		deck.shuffle();
		Card card2 = deck.getCardAt(3);	
		assertNotEquals(card1, card2);
	}

	@Test
	public void testDealMeAHandOfSize()
	{
		Deck deck = new Deck(false);
		Card[] cards = deck.dealMeAHandOfSize(HAND_SIZE);
		assertEquals(HAND_SIZE, cards.length);
		assertEquals(52-HAND_SIZE, deck.cardsLeft());
	}

}
