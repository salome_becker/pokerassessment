package com.salome.poker;

import static org.junit.Assert.*;

import org.dsaw.poker.engine.Card;
import org.dsaw.poker.engine.Hand;
import org.dsaw.poker.engine.HandValueType;
import org.junit.Before;
import org.junit.Test;

public class PokerHandEvaluatorSubClassTest
{

	private static final int HAND_SIZE = 5;

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void testIsTwoPair()
	{

		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("7 of Spades");
		cards[1] = new Card("7 of Hearts");
		cards[2] = new Card("6 of Clubs");
		cards[3] = new Card("5 of Diamonds");
		cards[4] = new Card("5 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.TWO_PAIRS, classUnderTest.getType());
	}
	
	@Test
	public void testIsOnePair()
	{

		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("7 of Spades");
		cards[1] = new Card("7 of Hearts");
		cards[2] = new Card("6 of Clubs");
		cards[3] = new Card("5 of Diamonds");
		cards[4] = new Card("2 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.ONE_PAIR, classUnderTest.getType());
	}
	
	@Test
	public void testIsHighCard()
	{
		// Simulate Straight hand of cards
		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("A of Spades");
		cards[1] = new Card("3 of Spades");
		cards[2] = new Card("5 of Hearts");
		cards[3] = new Card("8 of Hearts");
		cards[4] = new Card("10 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.HIGH_CARD, classUnderTest.getType());
	}

	@Test
	public void testIsStraight()
	{
		// Simulate Straight hand of cards
		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("A of Spades");
		cards[1] = new Card("K of Spades");
		cards[2] = new Card("J of Hearts");
		cards[3] = new Card("Q of Hearts");
		cards[4] = new Card("10 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.STRAIGHT, classUnderTest.getType());
	}

	@Test
	public void testIsFlush()
	{
		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("7 of Hearts");
		cards[1] = new Card("6 of Hearts");
		cards[2] = new Card("9 of Hearts");
		cards[3] = new Card("2 of Hearts");
		cards[4] = new Card("5 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.FLUSH, classUnderTest.getType());
	}

	@Test
	public void testIsFourOfAKind()
	{
		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("7 of Spades");
		cards[1] = new Card("7 of Hearts");
		cards[2] = new Card("7 of Clubs");
		cards[3] = new Card("7 of Diamonds");
		cards[4] = new Card("5 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.FOUR_OF_A_KIND, classUnderTest.getType());
	}

	@Test
	public void testIsStraightFlush()
	{
		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("7 of Hearts");
		cards[1] = new Card("6 of Hearts");
		cards[2] = new Card("4 of Hearts");
		cards[3] = new Card("3 of Hearts");
		cards[4] = new Card("5 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.STRAIGHT_FLUSH, classUnderTest.getType());
	}

	@Test
	public void testIsThreeOfAKind()
	{
		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("7 of Spades");
		cards[1] = new Card("7 of Hearts");
		cards[2] = new Card("7 of Clubs");
		cards[3] = new Card("6 of Diamonds");
		cards[4] = new Card("5 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.THREE_OF_A_KIND, classUnderTest.getType());
	}

	@Test
	public void testIsFullHouse()
	{
		Card[] cards = new Card[HAND_SIZE];
		cards[0] = new Card("7 of Spades");
		cards[1] = new Card("7 of Hearts");
		cards[2] = new Card("7 of Clubs");
		cards[3] = new Card("5 of Diamonds");
		cards[4] = new Card("5 of Hearts");

		Hand hand = new Hand(cards);
		PokerHandEvaluatorSubClass classUnderTest = new PokerHandEvaluatorSubClass(hand);
		assertEquals(HandValueType.FULL_HOUSE, classUnderTest.getType());
	}

}
