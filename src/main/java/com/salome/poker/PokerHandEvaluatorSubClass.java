package com.salome.poker;

import org.dsaw.poker.engine.Hand;
import org.dsaw.poker.engine.HandEvaluator;

/*
 * Implement this interface for the poker engine to allow changing the poker variant 
 * -  allow for changing of hand size 
 * -  allow for changing of how the hand ranks are evaluated
 */
public class PokerHandEvaluatorSubClass extends HandEvaluator
{
	
	public PokerHandEvaluatorSubClass(Hand hand)
	{
		super(hand);
	}

	/********************************************************
	 *  TODO - in case of use of another algorithm
	 *  		overwrite ranking methods 
	 *  		or 
	 *  		change in hand size
	 **************************8*****************************/
}
