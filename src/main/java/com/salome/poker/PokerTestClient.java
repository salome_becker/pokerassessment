package com.salome.poker;

import org.dsaw.poker.engine.Card;
import org.dsaw.poker.engine.Hand;
import org.dsaw.poker.engine.HandEvaluator;
import org.dsaw.poker.engine.HandValueType;

import com.salome.domain.cards.Deck;
import com.salome.factories.cards.DeckFactory;
import com.salome.factories.players.PlayerFactory;

public class PokerTestClient
{

	public static void main(String... args)
	{
		org.apache.log4j.PropertyConfigurator.configure("resources/log4j.properties");
		
		// instantiate deck of cards
		DeckFactory deckFactory = DeckFactory.getInstance();
		Deck deck = deckFactory.createDeck(false);
		
		// set hand size to deal
		int handSize = 5;
		
		// shuffle shuffle shuffle
		for (String arg: args)
		{
		  deck.shuffle();
		}  
		// deal cards
		Card[] cards = deck.dealMeAHandOfSize(handSize);
		System.out.print("**************Your hand: ");
		for (Card card : cards)
		{
			System.out.print(card.toString() + " ");
		}
		System.out.println();
		Hand hand1 = new Hand(cards);
		
		// evaluate hand of cards
		PokerHandEvaluatorSubClass evaluator = new PokerHandEvaluatorSubClass(hand1);
		int value = evaluator.getValue();
		HandValueType type = evaluator.getType();
		System.out.println("**************You have: " + type.getDescription());

		// TODO - not in current requirements but 
		// can extended for multi-players
		// PlayerFactory playerFactory;
		
		/* Also - the shuffling algorithm can be changed by 
		 * overriding HandEvaluator's ranking classes in the PokerHandEvaluatorSubClass */
		
	

	}
}
