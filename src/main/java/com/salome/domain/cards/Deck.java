package com.salome.domain.cards;

import org.apache.log4j.Logger;
import org.dsaw.poker.engine.Card;

/**
 * An object of type Deck represents a deck of playing cards. The deck is a
 * regular poker deck that contains 52 regular cards and that can also
 * optionally include two Jokers.
 */
public class Deck extends DeckAbstract
{
	public static Logger LOGGER = Logger.getLogger(Deck.class);

	public Deck(boolean includeJokers)
	{
		super(includeJokers);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.salome.cards.domain.DeckAbstract#shuffle()
	 */
	public void shuffle()
	{
		for (int i = 0; i < deck.length; i++)
		{
			int rand = (int) (Math.random() * (i + 1));
			Card temp = deck[i];
			deck[i] = deck[rand];
			deck[rand] = temp;
		}
		cardsUsed = 0;
	}

	public Card[] dealMeAHandOfSize(int handSize)
	{
		Card[] cards = new Card[handSize];
		for (int i = 0; i < handSize; i++)
		{
			cards[i] = dealCard();

		}
		return cards;

	}

	public Card getCardAt(int i)
	{
		return deck[i];
	}

} // end class Deck