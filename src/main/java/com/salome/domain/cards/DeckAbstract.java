package com.salome.domain.cards;

import org.apache.log4j.Logger;
import org.dsaw.poker.engine.Card;

public abstract class DeckAbstract
{
	
	public static Logger LOGGER = Logger.getLogger(DeckAbstract.class);
			
	protected static final int DECK_SIZE = 52;
	protected static final int NUM_JOKERS = 2;
	private static final int NUM_SUITES = 4;
	private static final int NUM_CARDS_PER_SUITE = 13;

	/**
	 * An array of 52 or 54 cards. A 54-card deck contains two Jokers, in
	 * addition to the 52 cards of a regular poker deck.
	 */
	protected Card[] deck;

	/**
	 * Keeps track of the number of cards that have been dealt from the deck so
	 * far.
	 */
	protected int cardsUsed;

	/**
	 * Constructs a regular 52-card poker deck. Initially, the cards are in a
	 * sorted order. The shuffle() method can be called to randomize the order.
	 * (Note that "new Deck()" is equivalent to "new Deck(false)".)
	 */
	public DeckAbstract()
	{
		this(false); // Just call the other constructor in this class.
	}

	/**
	 * Constructs a poker deck of playing cards, The deck contains the usual 52
	 * cards and can optionally contain two Jokers in addition, for a total of
	 * 54 cards. Initially the cards are in a sorted order. The shuffle() method
	 * can be called to randomize the order.
	 * 
	 * @param includeJokers
	 *            if true, two Jokers are included in the deck; if false, there
	 *            are no Jokers in the deck.
	 */
	public DeckAbstract(boolean includeJokers)
	{
		if (includeJokers)
			deck = new Card[DECK_SIZE+NUM_JOKERS];
		else
			deck = new Card[DECK_SIZE];
		int cardCount = 0; // How many cards have been created so far.
		for (int suit = 0; suit < NUM_SUITES; suit++)
		{
			for (int value = 0; value < NUM_CARDS_PER_SUITE; value++)
			{
				deck[cardCount] = new Card(value, suit);
				//System.out.println("Constructed card in deck: " + deck[cardCount].toString()); 
				LOGGER.info("Constructed card in deck: " + deck[cardCount].toString()); 
				cardCount++;
			}
		}
		if (includeJokers)
		{
			deck[DECK_SIZE] = new Card(1, Card.JOKER);
			deck[DECK_SIZE+1] = new Card(2, Card.JOKER);
		}
		cardsUsed = 0;
	}
	
	/**
	 * Put all the used cards back into the deck (if any), and shuffle the deck
	 * into a random order.  
	 * Implement this method with your shuffling algorithm
	 */
	public abstract void shuffle();
	
	/**
	 * As cards are dealt from the deck, the number of cards left decreases.
	 * This function returns the number of cards that are still left in the
	 * deck. The return value would be 52 or 54 (depending on whether the deck
	 * includes Jokers) when the deck is first created or after the deck has
	 * been shuffled. It decreases by 1 each time the dealCard() method is
	 * called.
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.salome.cards.domain.DeckAbstract#cardsLeft()
	 */
	public int cardsLeft()
	{
		return deck.length - cardsUsed;
	}

	/**
	 * Removes the next card from the deck and return it. It is illegal to call
	 * this method if there are no more cards in the deck. You can check the
	 * number of cards remaining by calling the cardsLeft() function.
	 * 
	 * @return the card which is removed from the deck.
	 * @throws IllegalStateException
	 *             if there are no cards left in the deck
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.salome.cards.domain.DeckAbstract#dealCard()
	 */
	public Card dealCard()
	{
		if (cardsUsed == deck.length)
			throw new IllegalStateException("No cards are left in the deck.");
		cardsUsed++;
		return deck[cardsUsed - 1];
		// Programming note: Cards are not literally removed from the array
		// that represents the deck. We just keep track of how many cards
		// have been used.
	}

	/**
	 * Test whether the deck contains Jokers.
	 * 
	 * @return true, if this is a 54-card deck containing two jokers, or false
	 *         if this is a 52 card deck that contains no jokers.
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.salome.cards.domain.DeckAbstract#hasJokers()
	 */
	public boolean hasJokers()
	{
		return (deck.length == DECK_SIZE + NUM_JOKERS);
	}

}