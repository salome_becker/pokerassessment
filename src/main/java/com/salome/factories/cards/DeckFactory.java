package com.salome.factories.cards;

import org.apache.log4j.Logger;
import org.dsaw.poker.engine.Card;
import org.dsaw.poker.engine.Hand;

import com.salome.domain.cards.Deck;

public class DeckFactory
{
	public static Logger LOGGER = Logger.getLogger(DeckFactory.class);

	protected static DeckFactory instance = null;

	public static synchronized DeckFactory getInstance()
	{
		if (instance == null)
		{
			instance = new DeckFactory();
		}
		return instance;
	}

	public Deck createDeck(boolean includeJokers)
	{
		Deck deck = new Deck(includeJokers);
		return deck;
	}



}
