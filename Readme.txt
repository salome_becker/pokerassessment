-Poker
-=====

USAGE NOTES:

1) Build Pre-requisites (this will all be done automatically in the run.sh script)
ANT_HOME must be set
If you don't have ant installed, you can find a copy in the tools folder
which you can extract and add the bin file to your PATH.


2) Runtime Pre-requisites:
JAVA_HOME must be set

3) CONFIGURATION:
A configuration file must be defined.  
See example property file, configuration.properties, in dist/config folder.

4) Run on commandline by using command:
      ./run.sh Shuffling ... Shuffling ... Shuffling ...  # this will build and run tests

5) LOGGING:
 is configured in log4j.properties
 is done to poker.log