if [ $# -gt 0 ]
then
  export ANT_HOME=tools/apache-ant-1.10.0
  export PATH="$PATH:$ANT_HOME/bin"
  ant all
  echo $1
  java -cp dist/Poker.jar:dist/log4j-1.2.17.jar:dist/junit-4.12.jar com.salome.poker.PokerTestClient $* 
else
  echo "USAGE EXAMPLE:  ./run.sh Shuffling ... Shuffling ... Shuffling ..."
fi
